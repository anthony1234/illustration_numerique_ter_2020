/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use rustnum::linear_algebra::operations::*;
use rustnum::linear_algebra::dense::Dense;
use rustnum::linear_algebra::lower_triangular::LowerTriangular;
use rustnum::linear_algebra::upper_triangular::UpperTriangular;

pub fn resoudre_systeme_lineaire(nombre_domaines: usize, n: usize, a: Dense<f64>, b: Dense<f64>) -> Vec<f64>
{
    /*
    Résoud le système linéaire et renvoie les valeurs approximées
    */

    let (l, u) = decomposition::lu::par_dense(&a);

    let mut aux1 = Dense::<f64>::zeros(n*nombre_domaines,1);
    let mut aux2 = Dense::<f64>::zeros(n*nombre_domaines,1);

    solve::lower_triangular::default(&l, &b, &mut aux1);
    solve::upper_triangular::default(&u, &aux1, &mut aux2);

    return aux2.v;
}