/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

mod donnees;
mod creer_maillage;
mod former_matrice;
mod former_second_membre;
mod resoudre_systeme_lineaire;
mod reconstruction;
mod test;
mod graph;

use crate::donnees::*;
use crate::creer_maillage::*;
use crate::former_matrice::*;
use crate::former_second_membre::*;
use crate::resoudre_systeme_lineaire::*;
use crate::reconstruction::*;
use crate::test::*;
use crate::graph::*;
use std::fs::File;
use std::io::prelude::*;

fn main() {
    /* Ce code correspond au tracé du graph de l'erreur en fonction de n
    les résultats sont écrits dans le fichier donnees_erreur2.dat */
    
    // Nombre de domaines
    const N: usize = 2;

    // Nombre de points de discrétisation
    let mut n: usize = 1000;
    //let mut erreur: f64;
    //let mut file = File::create("donnees_erreur2.dat").unwrap();
    
    //println!("n = {}", n);

    let s = creer_maillage(n);

    let a = par_former_matrice(N, n, &s);

    let b = former_second_membre(N, n, &s);

    let q = resoudre_systeme_lineaire(N, n, a, b);

    //println!("{}", reconstruction(N,n,&s,&q,[0f64,0f64]));
    println!("{}", test(-0.5f64,0.5f64,-0.5f64,0.3f64,10,10,N,n,&s,&q));

    /*for x in q 
    {
        println!("{}", x);
    }*/
}
