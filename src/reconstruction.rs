/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::donnees::*;
use std::f64::consts::*;

pub fn reconstruction(nombre_domaines: usize, n: usize, s: &Vec<f64>, q: &Vec<f64>, x: [f64;2]) -> f64 
{
    /*
    Reconstruit la solution de l'EDP au point x 
    */

    let mut somme = 0f64;
    let mut r_i_s_k: [f64;2];
    let mut aux: f64;
    let moins_1_sur_4_pi: f64 = -1f64/(4f64*PI);

    for i in 1..(nombre_domaines+1)
    {
        for k in 0..n
        {
            r_i_s_k = r(i,s[k]);

            aux = ((x[0]-r_i_s_k[0]).powi(2) + (x[1]-r_i_s_k[1]).powi(2)).ln();

            somme = somme + (s[k+1]-s[k])*q[k+n*(i-1)]*norme_r_point(i,s[k])*aux;
        }
    }

    return moins_1_sur_4_pi*somme;
}