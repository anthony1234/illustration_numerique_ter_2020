/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use rustnum::linear_algebra::dense::Dense;
use crate::donnees::*;
use rayon::prelude::*;
use std::sync::{Mutex, Arc};
use std::f64::consts::*;

pub fn former_matrice(nombre_domaines: usize, n: usize, s: &Vec<f64>) -> Dense<f64>
{
    /*
    Forme la matrice du système linéaire à résoudre
    */

    let mut a = Dense::<f64>::zeros(n*nombre_domaines,n*nombre_domaines);

    let mut aux: f64;
    let mut somme = 0f64;
    let mut p_f64: f64;
    let mut r_i_s_k: [f64;2];
    let mut r_j_s_l: [f64;2];

    let moins_1_sur_2_pi: f64 = -1f64/(2f64*PI);
    let moins_1_sur_4_pi: f64 = -1f64/(4f64*PI);

    for i in 1..(nombre_domaines+1)
    {
        for k in 0..n 
        {
            for j in 1..i
            {
                for l in 0..n 
                {   
                    r_i_s_k = r(i,s[k]);
                    r_j_s_l = r(j,s[l]);

                    aux = moins_1_sur_4_pi*((r_i_s_k[0]-r_j_s_l[0]).powi(2) + (r_i_s_k[1]-r_j_s_l[1]).powi(2)).ln()*norme_r_point(j, s[l])*(s[l+1]-s[l]);

                    a.fast_set(k+n*(i-1),l+n*(j-1), aux);
                }
            }

            for l in 0..n 
            {
                somme = 0f64;

                for p in 1..(n+1)
                {
                    p_f64 = p as f64;

                    somme = somme + ((p_f64*s[l]).cos()*(p_f64*s[k]).cos() + (p_f64*s[l]).sin()*(p_f64*s[k]).sin())/p_f64;
                }

                    
                aux = moins_1_sur_2_pi*norme_r_point(i, s[l])*(s[l] - s[l+1])*(0.5f64 - b(i,s[l], s[k]) + somme);

                a.fast_set(k+n*(i-1),l+n*(i-1), aux);
            }

            for j in (i+1)..(nombre_domaines+1)
            {
                for l in 0..n 
                {
                    r_i_s_k = r(i,s[k]);
                    r_j_s_l = r(j,s[l]);

                    aux = moins_1_sur_4_pi*((r_i_s_k[0]-r_j_s_l[0]).powi(2) + (r_i_s_k[1]-r_j_s_l[1]).powi(2)).ln()*norme_r_point(j, s[l])*(s[l+1]-s[l]);

                    a.fast_set(k+n*(i-1),l+n*(j-1), aux);
                }
            }
        }
    }

    return a;
}

pub fn par_former_matrice(nombre_domaines: usize, n: usize, s: &Vec<f64>) -> Dense<f64>
{
    /*
    Forme la matrice du système linéaire à résoudre avec une implémentation parallèle
    */

    let mut a = Arc::new(Mutex::new(Dense::<f64>::zeros(n*nombre_domaines,n*nombre_domaines)));

    let iterator: Vec<usize> = (0..n).collect();
    
    let moins_1_sur_2_pi: f64 = -1f64/(2f64*PI);
    let moins_1_sur_4_pi: f64 = -1f64/(4f64*PI);

    for i in 1..(nombre_domaines+1)
    {
        iterator.par_iter().for_each(|k|
        {
            let mut aux: f64;
            let mut somme = 0f64;
            let mut p_f64: f64;
            let mut r_i_s_k: [f64;2];
            let mut r_j_s_l: [f64;2];

            for j in 1..i
            {
                for l in 0..n 
                {   
                    r_i_s_k = r(i,s[*k]);
                    r_j_s_l = r(j,s[l]);

                    aux = moins_1_sur_4_pi*((r_i_s_k[0]-r_j_s_l[0]).powi(2) + (r_i_s_k[1]-r_j_s_l[1]).powi(2)).ln()*norme_r_point(j, s[l])*(s[l+1]-s[l]);

                    a.lock().unwrap().fast_set(*k+n*(i-1),l+n*(j-1), aux);
                }
            }

            for l in 0..n 
            {
                somme = 0f64;

                for p in 1..(n+1)
                {
                    p_f64 = p as f64;

                    somme = somme + ((p_f64*s[l]).cos()*(p_f64*s[*k]).cos() + (p_f64*s[l]).sin()*(p_f64*s[*k]).sin())/p_f64;
                }

                    
                aux = moins_1_sur_2_pi*norme_r_point(i, s[l])*(s[l] - s[l+1])*(0.5f64 - b(i,s[l], s[*k]) + somme);

                a.lock().unwrap().fast_set(*k+n*(i-1),l+n*(i-1), aux);
            }

            for j in (i+1)..(nombre_domaines+1)
            {
                for l in 0..n 
                {
                    r_i_s_k = r(i,s[*k]);
                    r_j_s_l = r(j,s[l]);

                    aux = moins_1_sur_4_pi*((r_i_s_k[0]-r_j_s_l[0]).powi(2) + (r_i_s_k[1]-r_j_s_l[1]).powi(2)).ln()*norme_r_point(j, s[l])*(s[l+1]-s[l]);
                    
                    a.lock().unwrap().fast_set(*k+n*(i-1),l+n*(j-1), aux);
                }
            }
        })
    }

    return Arc::try_unwrap(a).unwrap().into_inner().unwrap();
}