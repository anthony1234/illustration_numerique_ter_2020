/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::f64::consts::*;
const TOL: f64 = 0.000001;

pub fn f(x: f64, y: f64) -> f64
{
    /*
    Fonction second membre du problème initial
    */

    return x*y;
}

pub fn g_tilde(i: usize, s: f64) -> f64 
{
    /*
    \tilde{g} = f \circ r

    i est l'indice du domaine
    */
    //let r_v = r(s);
    if i == 1
    {
        return 1f64-2f64*s.sin().powi(2);
    }
    if i == 2
    {
        return 0.01*(1f64-2f64*s.sin().powi(2))+0.1*(s.cos()-s.sin());
    }
    else
    {
        panic!("Indice ne correspondant a aucun domaine.")
    }
}

pub fn r(i: usize, t: f64) -> [f64; 2]
{
    /*
    Paramétrisation de \Gamma

    On doit avoir t \in [0, 2 \pi]

    i est l'indice du domaine
    */
    if i == 1
    {
        return [t.cos(), t.sin()];
    }
    if i == 2 
    {
        return [0.1*t.cos()+0.5, 0.1*t.sin()+0.5]
    }
    else
    {
        panic!("Indice ne correspondant a aucun domaine.")
    }
}

pub fn r_point(i: usize, t: f64) -> [f64; 2]
{
    /*
    Paramétrisation de \Gamma

    On doit avoir t \in [0, 2 \pi]

    i est l'indice du domaine
    */
    if i == 1
    {
        return [-t.sin(), t.cos()];
    }
    if i == 2
    {
        return [-0.1*t.sin(), 0.1*t.cos()]
    }
    else
    {
        panic!("Indice ne correspondant a aucun domaine.")
    }
}

pub fn norme_r_point(i: usize, t: f64) -> f64
{
    /*
    Paramétrisation de \Gamma

    On doit avoir t \in [0, 2 \pi]

    i est l'indice du domaine
    */
    if i == 1
    { 
        return 1f64;
    }
    if i == 2
    {
        return 0.1f64;
    }
    else
    {
        panic!("Indice ne correspondant a aucun domaine.")
    }
}

pub fn b(i: usize, s_1: f64, s_2: f64) -> f64 
{
    /*
    Fonction B utilisée dans la décomposition du noyau

    s_1, s_2 \in [0, 2 \pi]
    */

    if (s_1 - s_2).abs() > TOL 
    {
        let r_i_s_1 = r(i,s_1);
        let r_i_s_2 = r(i,s_2);

        let aux: f64 = ((r_i_s_1[0]-r_i_s_2[0]).powi(2)+(r_i_s_1[1]-r_i_s_2[1]).powi(2)).sqrt();

        return ((E.sqrt()*(aux))/(2f64*(0.5f64*(s_1-s_2)).sin()).abs()).ln();
    }

    
    return (E.sqrt()*norme_r_point(i,s_1)).ln();
}

pub fn u_analytique(x: [f64;2]) -> f64 
{
    /*
    Solution analytique du problème
    */
    
    return x[0].powi(2)-x[1].powi(2);
}