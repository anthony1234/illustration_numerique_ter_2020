/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use rustnum::linear_algebra::dense::Dense;
use crate::donnees::*;
use std::f64::consts::*;

pub fn former_second_membre(nombre_domaines: usize, n: usize, s: &Vec<f64>) -> Dense<f64> 
{
    /*
    Forme le second membre du système linéaire à résoudre
    */

    let mut sec = Dense::<f64>::zeros(n*nombre_domaines,1);

    for i in 1..(nombre_domaines+1)
    {
        for k in 0..n 
        {
            sec.fast_set(k+n*(i-1),0, g_tilde(i,s[k]));
        }
    }

    return sec;
}