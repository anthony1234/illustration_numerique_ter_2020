/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

// Constantes 
use std::f64::consts::*;

pub fn creer_maillage(n: usize) -> Vec<f64>
{
    /*
    Génère le maillage de [0, 2 \pi] de sorte que s_0 = 0 et s_n = 2 \pi
    */

    let n_f64: f64 = n as f64;
    let mut s: Vec<f64> = vec![0.0; n+1];
    let mut i: f64 = 0f64;

    for v in s.iter_mut()
    {
        *v = i*(2f64*PI)/n_f64;

        i = i + 1f64;
    }

    return s;
}