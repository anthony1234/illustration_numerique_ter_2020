/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::fs::File;
use crate::reconstruction::*;
use std::io::prelude::*;
use std::string::String;

pub fn graph(x_min: f64, x_max: f64, y_min: f64, y_max: f64, n_x: usize, n_y: usize, nombre_domaines: usize, n: usize, q: &Vec<f64>, s: &Vec<f64>)
{
    /*
    Génère un fichier .dat en vu de faire un tracé
    */

    let mut x: [f64;2] = [x_min, y_min];
    let delta_x: f64 = (x_max-x_min)/(n_x as f64);
    let delta_y: f64 = (y_max-y_min)/(n_y as f64);
    let mut valeur: f64;

    let mut file = File::create("donnees.dat").unwrap();

    for i in 0..n_x 
    {
        for j in 0..n_y 
        {
            valeur = reconstruction(nombre_domaines,n, &s, &q, x);

            writeln!(&mut file, "{} {} {}", x[0], x[1], valeur);

            x[0] = x[0] + delta_x;
        }
        x[1] = x[1] + delta_y;
        x[0] = x_min;
    }
}