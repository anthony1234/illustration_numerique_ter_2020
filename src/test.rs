/*
Copyright 2020 Anthony Gerber-Roth
This file is part of illustration_numerique_ter_2020.

illustration_numerique_ter_2020 is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

illustration_numerique_ter_2020 is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with illustration_numerique_ter_2020.  If not, see <https://www.gnu.org/licenses/>.
*/

use crate::donnees::*;
use crate::reconstruction::*;

pub fn test(x_min: f64, x_max: f64, y_min: f64, y_max: f64, n_x: usize, n_y: usize, nombre_domaines: usize, n: usize, s: &Vec<f64>, q: &Vec<f64>) -> f64 
{
    /*
    Calcul l'erreur commise sur un ensemble de points au sens de 

    erreur = max |u_an - u_num|
    */

    let mut x: [f64;2] = [x_min, y_min];
    let delta_x: f64 = (x_max-x_min)/(n_x as f64);
    let delta_y: f64 = (y_max-y_min)/(n_y as f64);
    let mut valeur: f64;
    let mut erreur: f64;
    let mut max: f64 = 0f64;

    for i in 0..n_x 
    {
        for j in 0..n_y 
        {
            erreur = (reconstruction(nombre_domaines,n, &s, &q, x) - u_analytique(x)).abs();
            println!("{}",erreur);
            if erreur > max 
            {
                max = erreur;
            }
            
            x[0] = x[0] + delta_x;
        }
        x[1] = x[1] + delta_y;
        x[0] = x_min;
    }

    return max;
}